#!/bin/bash

## 02_01_install_openjdk on ubuntu
bash pwd
echo "RUNNING 02_01_install_openjdk.sh"
echo "sudo apt -y install openjdk-8-jdk"
sudo apt -y install openjdk-8-jdk
echo "cd /usr/lib/jvm/java-8-openjdk-amd64"
cd /usr/lib/jvm/java-8-openjdk-amd64
# sudo apt -y install openjdk-11-jdk
# cd /usr/lib/jvm/java-11-openjdk-amd64
