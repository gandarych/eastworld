package pl.com.microservices.eastworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class EastworldApplication {

	public static void main(String[] args) {
		SpringApplication.run(EastworldApplication.class, args);
	}

	@GetMapping("/hollo")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
		return String.format("Hollo %s! Eastworld app is alive!", name);
	}

}
